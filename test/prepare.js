/**
 * Created by lubuntu on 19/06/17.
 */

const prepare = require('mocha-prepare');
const exec    = require('child_process').exec;

prepare(function (done) {
  process.env.NODE_ENV = 'test';

  const config = require(`${__dirname}/../server/config/database/sequelize.json`)['test'];
  const Sequelize = require('sequelize');
  const sequelize = new Sequelize(config.database, config.username, config.password, config);

  sequelize
  .query(`drop owned by wesfel;`)
  .then(() => {
    // Running db migrations
    exec('cd ' + __dirname + '/.. && sequelize db:migrate --env test', function(err, stdout) {

      console.log(stdout);

      // Populate db with default data
      require('../server/config/bootstrap')(() => {
        var app = require('../app');
        app.listen(3000);

        done();
      });
    })
  })
  .catch((err) => {
    done(err);
  });
}, function (done) {
  // called after all test completes (regardless of errors){
    done();
});