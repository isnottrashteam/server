const supertest = require("supertest");
const should    = require("should");

describe('app', () => {

  describe("app unit test", () => {

    var server = supertest.agent("http://localhost:3000/api");

    it("should add two number", function (done) {
      //calling ADD api
      server
        .get('/ping')
        .expect(200)
        .end(function (err, res) {
          res.text.should.equal('pong');
          done();
        });
    });
  });
});