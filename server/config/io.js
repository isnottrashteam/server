/**
 * Created by lubuntu on 14/04/17.
 */

const sessionService = require('./session'),
        cookieParser = require('cookie-parser'),
              config = require('./config'),
                uuid = require('uuid/v4');

io.use(function (socket, next) {
  var parseCookie = cookieParser(config.sessionSecret);
  var handshake = socket.request;

  parseCookie(handshake, null, function (err, data) {
    sessionService.get(handshake, function (err, session) {

      if (err) next(new Error(err.message));
      if (!session) {
        next(new Error("Not authorized"));
      }
      else {
        handshake.session = session;

        console.log(sessionService.getSessionId(handshake));

        const connectionId = sessionService.getSessionId(handshake);

        var room = uuid();
        socket.join(room);
        socket.join('global');
        socket.join(session.passport.user.role);

        sessionService.storeSession(session.passport.user.id, connectionId, socket.id, room);
        next();
      }
    });
  });
});

io.sockets.on('connection', function (socket) {
  var userId = socket.request.session.passport.user.id;
  var userName = socket.request.session.passport.user.nome;
  var socketId = socket.id;

  socket.once('disconnect', function (r) {
    sessionService.destroySession(userId, socketId, () => {
      console.log(r)
    });
  });

  socket.on('readMessage', function () {
    // TODO use redis to check count value
    ChatService.read(userId);
  });

  socket.on('sendMessage', function (message) {
    const messageMeta = {
      authorId: userId,
      authorName: userName,
      timestamp: Date.now(),
      message: message
    };

    socket.broadcast.to('global').emit('chatMessage', messageMeta);
    ChatService.persist(messageMeta);
  })
});
