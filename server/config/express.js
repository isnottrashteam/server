/**
 * Created by lubuntu on 01/04/17.
 */

const cookieParser = require('cookie-parser'),
        bodyParser = require('body-parser'),
           express = require('express'),
            config = require('./config.js'),
            logger = require('morgan'),
              path = require('path');

const app = express();

// Log requests to the console.
app.use(logger('dev'));

if (config.cors) {
  app.all('*', function (req, res, next) {
    var origin = req.get('origin');
    res.header('Access-Control-Allow-Origin', origin);
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Authorization');
    next();
  });
}

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

module.exports = app;
