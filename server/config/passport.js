const passport = require('passport'),
       session = require('express-session'),
        bcrypt = require('bcrypt'),
        config = require('./database/redis.json')[process.env.NODE_ENV || 'development'],
         redis = require('redis');

const LocalStrategy = require('passport-local').Strategy,
         RedisStore = require('connect-redis')(session),
             client = redis.createClient();

module.exports = function (app) {
  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    use.models.User.findOne({where: {id: user.id}}).then((user) => {
      done(null, user);
    }).catch((err) => done(err));
  });

  passport.use('local-login', new LocalStrategy({
      usernameField: 'username',
      passwordField: 'password'
    },
    function(username, password, done) {
      use.models.User.findOne({where: { username: username }}).then((user) => {

        if (!user) {
          return done(null, false, { message: 'Incorrect username.' });
        }

        user.comparePassword(password, (err, res) => {
          if (!res)
            return done(null, false, {
              message: 'Invalid Password'
            });

          var returnUser = {
            nome: user.nome,
            username: user.username,
            role: user.role,
            id: user.id
          };

          return done(null, returnUser, {
            message: 'Logged In Successfully'
          });
        });
      }).catch((err) => {
        return done(err);
      });
    }
  ));

  const store = new RedisStore({ host: config.host, port: config.port, client: client, ttl:  config.ttl });

  app.use(session({
    store: new RedisStore({ host: config.host, port: config.port, client: client, ttl: 8 * 60 * 60 * 1000 }),
    store: store,
    secret: 'FC1!dNMZVeluAg9N$0hVFYDbPeKUZSx5KeYI$FxVvBXj8oqh3iK2yt?edzfFitsJVrnU_',
    resave: false,
    saveUninitialized: false
  }));

  global.redisStore = store;

  require('./session').initializeRedis(client, store);

  app.use(passport.initialize());
  app.use(passport.session());

  return passport;
};
