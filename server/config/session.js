/**
 * Created by lubuntu on 14/04/17.
 */

const config = require('./config'),
           _ = require('lodash');

var redisClient = null;
var redisStore = null;

var self = module.exports = {
  initializeRedis: function (client, store) {
    redisClient = client;
    redisStore = store;
  },

  getSessionId: function (handshake) {
    return handshake.signedCookies[config.sessionCookieKey];
  },

  get: function (handshake, callback) {
    var sessionId = self.getSessionId(handshake);

    self.getSessionBySessionID(sessionId, function (err, session) {
      if (err) callback(err);
      if (callback != undefined)
        callback(null, session);
    });
  },

  getSessionBySessionID: function (sessionId, callback) {
    redisStore.load(sessionId, function (err, session) {
      if (err) callback(err);
      if (callback != undefined)
        callback(null, session);
    });
  },

  getUserName: function (handshake, callback) {
    self.get(handshake, function (err, session) {
      if (err) callback(err);
      if (session)
        callback(null, session.userName);
      else
        callback(null);
    });
  },

  destroySession: function (id, sessionId, cb) {
    self.getSession(id, (values) => {
      if (values && values[1]) {
        var index = values[1].indexOf(sessionId);

        if (index !== -1) {
          values[1].splice(index, 1);
        }

        redisClient.hmset('rt:' + id, {'room': values[0], 'sockets': JSON.stringify(values[1])}, cb);
      }
    });
  },

  storeSession: function (id, connectionId, sessionId, room, cb) {
    self.getSession(id, (values) => {
      if (values && values[1]) {
        values[1].push(sessionId);
        redisClient.hmset('rt:' + id, {'room': values[0], 'sockets': JSON.stringify(values[1])}, cb);
      } else {
        redisClient.hmset('rt:' + id, {'room': room, 'sockets': JSON.stringify([sessionId])}, cb);
        redisClient.expire('rt:' + id, 60*10);

        setTimeout(function () {
          self.getSession(id, (values) => {

            if (values && values[1]) {
              for (let i = 0; i < values[1].length; i++) {
                let socket = io.sockets.connected[values[1][i]];

                if (socket) {
                  socket.disconnect(401);
                }
              }
            }
          });
        }, 60 * 10 * 1000);
      }
    });
  },

  getSession: function (id, cb) {
    redisClient.hmget('rt:' + id, 'room', 'sockets', (e, values) => {
      if (values) {
        cb([values[0], JSON.parse(values[1])]);
      } else {
        cb();
      }
    });
  },

  updateSession: function (session, callback) {
    try {
      session.reload(function () {
        session.touch().save();
        callback(null, session);
      });
    }
    catch (err) {
      callback(err);
    }
  },

  setSessionProperty: function (session, propertyName, propertyValue, callback) {
    session[propertyName] = propertyValue;
    self.updateSession(session, callback);
  }
};