/**
 * Created by lubuntu on 16/06/17.
 */

exports.install = () => {
    use.route('/api/ping', get, ['get']);
};

function get (req, res) {
  res.status(200).send("pong");
}