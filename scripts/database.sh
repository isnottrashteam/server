#!/usr/bin/env bash

function installPostgresql {
    sudo apt install -y postgresql pgadmin3
}

function installRedis {
    sudo apt install -y redis-server
}

function createUser {
    sudo -u postgres bash -c "psql -c \"CREATE USER wesfel WITH PASSWORD '147Password#123';\""
    echo "create user done!"
}

function createDevDatabase {
    sudo -u postgres bash -c "psql -c \"CREATE DATABASE wesfel_dev;\""
    echo "create dev database done!"
}

function createTestDatabase {
    sudo -u postgres bash -c "psql -c \"CREATE DATABASE wesfel_test;\""
    echo "create test database done!"
}

function resetDev {
    sudo -u postgres bash -c "psql -c \"DROP DATABASE wesfel_dev;\""
    sudo -u postgres bash -c "psql -c \"CREATE DATABASE wesfel_dev;\""
    echo "reset dev done!"
}

function resetTest {
    sudo -u postgres bash -c "psql -c \"DROP DATABASE wesfel_test;\""
    sudo -u postgres bash -c "psql -c \"CREATE DATABASE wesfel_test;\""
    echo "reset test done!"
}

bold=$(tput bold)
normal=$(tput sgr0)

function help {
    echo
    echo "${bold}## Usage ##${normal}"
    echo
    echo "database.sh [-i install] [-c configure] [-r reset]"
    echo
    echo "${bold}## Parameters ##${normal}"
    echo
    echo "${bold}-i, --install${normal}"
    echo
    echo "${bold}pg${normal}: postrgresql"
    echo "${bold}redis${normal}: redis"
    echo
    echo "${bold}-c, --configure${normal}"
    echo
    echo "[dev|test|all]"
    echo
    echo "${bold}-rm --reset${normal}"
    echo
    echo "[dev|test|all]"
    echo
    echo "${bold}## Example ##${normal}"
    echo
    echo "database.sh -i=pg -c=dev"
    echo
}

if [ $# = 0 ]
    then
       help
fi

for i in "$@"
    do
    case $i in
        -i=*|--install=*)
            INSTALL="${i#*=}"
        ;;
        -c=*|--config=*)
            CONFIGURE="${i#*=}"
        ;;
        -r=*|--reset=*)
            RESET="${i#*=}"
        ;;
        -h|--help)
          help
          exit 0
        ;;
        --default)
            DEFAULT=YES
        ;;
        *)
           # unknown option
        ;;
    esac
done

PG="pg"
REDIS="redis"
ALL="all"

case ${INSTALL} in
    ${PG})
        installPostgresql
    ;;
    ${REDIS})
        installRedis
    ;;
    ${ALL})
        installPostgresql && installRedis
    ;;
esac

DEV="dev"
TEST="test"
                
case ${CONFIGURE} in
    ${DEV})
        createUser && createDevDatabase
    ;;
    ${TEST})
        createUser && createTestDatabase
    ;;
    ${ALL})
        createUser && createDevDatabase && createTestDatabase
    ;;
esac

case ${RESET} in
    ${DEV})
        resetDev
    ;;
    ${TEST})
        resetTest
    ;;
    ${ALL})
        resetDev && resetTest
    ;;
esac
