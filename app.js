const path = require('path'),
         _ = require('lodash'),
       app = require('./server/config/express');

global.passport = require('./server/config/passport')(app);

global.use = {
  route: (route, func, options) => {
    if (typeof options == 'undefined' || options.length == 0) {
      options = ['get'];
    }

    var hooks = [];

    for (var i = 0; i < options.length - 1; i++) {
      if (_.isString(options[i])) {
        hooks.push(require('./server/hooks/' + options[i]));
      }
    }

    hooks.push(func);

    app[options[options.length - 1]](route, hooks);
  },

  models: require('./server/models')
};

// Require our routes into the application.
//require('./server/routes')(app);

// Require our services into the application
const controllersPath = path.join(__dirname, 'server', 'controllers');

require('fs').readdirSync(controllersPath).forEach(function(file) {
  var module = require(path.join(controllersPath, file));

  if (module.install) {
    module.install();
  }
});

// Require our services into the application
const servicesPath = path.join(__dirname, 'server', 'services');

require('fs').readdirSync(servicesPath).forEach(function(file) {
  global[file.split('.')[0]] = require(path.join(servicesPath, file));
});

require('./server/config/bootstrap')();

module.exports = app;